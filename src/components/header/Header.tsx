import './Header.scss';
import {Link} from 'react-router-dom';
import {ReactComponent as Logo} from '../../assets/crown.svg';
import {auth} from '../../firebase/firebase.utils';
import {FunctionComponent} from 'react';
import {connect} from 'react-redux';

interface IHeaderProps {
  currentUser?: any;
}
const Header: FunctionComponent<IHeaderProps> = ({currentUser}) => {
  return (
    <div className="header">
      <Link className="logo-container" to="/">
        <Logo className="logo" />
      </Link>
      <div className="options">
        <Link className="option" to="/shop">
          SHOP
        </Link>
        <Link className="option" to="/shop">
          CONTACT
        </Link>
        {currentUser ? (
          <div className="option" onClick={() => auth.signOut()}>
            SIGN OUT
          </div>
        ) : (
          <Link to="signin">SIGN IN</Link>
        )}
      </div>
    </div>
  );
};

const mapStateToProps = (state: IHeaderProps) => ({
  currentUser: state.currentUser,
});

export default connect(mapStateToProps, null)(Header);
