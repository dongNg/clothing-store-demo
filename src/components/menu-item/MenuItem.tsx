import {FunctionComponent} from 'react';
import {withRouter} from 'react-router';
import {RouteComponentProps} from 'react-router-dom';
import {IMenuItem} from '../../models';
import './MenuItem.scss';

interface PropsMenuItem extends RouteComponentProps {
  menuItem: IMenuItem;
}
const MenuItem: FunctionComponent<PropsMenuItem> = ({
  menuItem,
  history,
  match,
}) => {
  return (
    <div
      className={`${menuItem.size} menu-item`}
      onClick={() => history.push(`${match.url}${menuItem.linkUrl}`)}
    >
      <div
        className="background-image"
        style={{backgroundImage: `url(${menuItem.imageUrl})`}}
      />
      <div className="content">
        <h1 className="title">{menuItem.title?.toLocaleUpperCase()}</h1>
        <span className="subtitle">SHOW NOW</span>
      </div>
    </div>
  );
};

export default withRouter(MenuItem);
