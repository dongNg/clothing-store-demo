import './SignIn.scss';
import {useState, SyntheticEvent, FunctionComponent} from 'react';
import {ILogin} from '../../models';
import {CustomButton, FormInput} from '../index';
import {auth, signInWithGoogle} from '../../firebase/firebase.utils';
const SignIn: FunctionComponent = () => {
  // eslint-disable-next-line
  const [state, setState] = useState<ILogin>({
    email: '',
    password: '',
  });

  const handleSubmit = async (event: SyntheticEvent) => {
    event.preventDefault();
    const {email, password}: any = state;
    try {
      await auth.signInWithEmailAndPassword(email, password);
      setState({email: '', password: ''});
    } catch (error) {
      console.log(error);
    }
  };

  const handleChange = (event: SyntheticEvent) => {
    const {value, name} = event.target as HTMLInputElement;

    setState({...state, [name]: value});
  };
  return (
    <div className="sign-in">
      <h2>I already hava an account</h2>
      <span>Sign in with your email and password</span>

      <form onSubmit={e => handleSubmit(e)}>
        <FormInput
          label="Email"
          name="email"
          type="email"
          value={state.email}
          required
          handleChange={e => handleChange(e)}
        />
        <FormInput
          label="Password"
          name="password"
          type="password"
          value={state.password}
          required
          handleChange={e => handleChange(e)}
        />
        <div className="buttons">
          <CustomButton type="submit">Submit Form</CustomButton>
          <CustomButton type="button" onClick={signInWithGoogle} isGoogleSignIn>
            Submit Form Google
          </CustomButton>
        </div>
      </form>
    </div>
  );
};

export default SignIn;
