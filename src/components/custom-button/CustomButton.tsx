import {DetailedHTMLProps, FunctionComponent} from 'react';
import './CustomButton.scss';

interface ICustomButton
  extends DetailedHTMLProps<
    React.ButtonHTMLAttributes<HTMLButtonElement>,
    HTMLButtonElement
  > {
  isGoogleSignIn?: boolean;
}
const CustomButton: FunctionComponent<ICustomButton> = ({
  children,
  isGoogleSignIn,
  ...otherProps
}) => {
  return (
    <button
      className={`${isGoogleSignIn ? 'google-sign-in' : ''} custom-button`}
      {...otherProps}
    >
      {children}
    </button>
  );
};

export default CustomButton;
