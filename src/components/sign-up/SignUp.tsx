import {FunctionComponent, useState, SyntheticEvent} from 'react';
import {CustomButton, FormInput} from '..';
import {auth, createUserProfileDocument} from '../../firebase/firebase.utils';
import './SignUp.scss';
const SignUp: FunctionComponent = () => {
  // eslint-disable-next-line
  const [signState, setSignState] = useState({
    displayName: '',
    email: '',
    password: '',
    confirmPassword: '',
  });

  const handleChange = (event: SyntheticEvent) => {
    const {value, name} = event.target as HTMLInputElement;
    setSignState({...signState, [name]: value});
  };

  const handleSubmit = async (event: SyntheticEvent) => {
    event.preventDefault();
    const {displayName, email, password, confirmPassword} = signState;
    if (password !== confirmPassword) {
      alert("Password don't match");
      return;
    }

    try {
      const {user} = await auth.createUserWithEmailAndPassword(email, password);

      await createUserProfileDocument(user, {displayName});
      setSignState({
        displayName: '',
        email: '',
        password: '',
        confirmPassword: '',
      });
    } catch (error) {
      console.log(error);
    }
  };
  return (
    <div className="sign-up">
      <div className="title">I do not have a account</div>
      <span>Sign up with your email and password</span>
      <form className="sign-up-form" onSubmit={handleSubmit}>
        <FormInput
          type="text"
          name="displayName"
          label="Display Name"
          value={signState.displayName}
          required
          handleChange={handleChange}
        />
        <FormInput
          type="email"
          name="email"
          label="Email"
          value={signState.email}
          required
          handleChange={handleChange}
        />
        <FormInput
          type="password"
          name="password"
          label="Password"
          value={signState.password}
          required
          handleChange={handleChange}
        />
        <FormInput
          type="password"
          name="confirmPassword"
          label="Confirm Password"
          value={signState.confirmPassword}
          required
          handleChange={handleChange}
        />
        <CustomButton type="submit">SIGN UP</CustomButton>
      </form>
    </div>
  );
};

export default SignUp;
