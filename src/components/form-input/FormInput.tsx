import {FunctionComponent, SyntheticEvent} from 'react';
import './FormInput.scss';
interface IInputProps {
  name?: string;
  label?: string;
  type?: string;
  value?: any;
  required?: boolean;
  handleChange: (e: SyntheticEvent) => any;
}
const FormInput: FunctionComponent<IInputProps> = ({
  handleChange,
  label,
  name,
  type,
  ...otherProps
}) => {
  return (
    <div className="group">
      <input
        className="form-input"
        type={type}
        name={name}
        onChange={handleChange}
        {...otherProps}
      />
      {label ? (
        <label
          className={`${
            otherProps.value.length ? 'shrink' : ''
          } form-input-label`}
        ></label>
      ) : null}
    </div>
  );
};

export default FormInput;
