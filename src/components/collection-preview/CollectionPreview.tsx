import {FunctionComponent} from 'react';
import {ICollectionPreview} from '../../models';
import CollectionItem from '../collection-item/CollectionItem';
import './CollectionPreview.scss';

const CollectionPreview: FunctionComponent<ICollectionPreview> = ({
  title,
  items,
}) => {
  return (
    <div className="collection-preview">
      <h1 className="title">{title}</h1>
      <div className="preview">
        {items
          ?.filter((item, idx) => idx < 4)
          .map(({id, ...otherCollectionProps}) => (
            <CollectionItem key={id} {...otherCollectionProps} />
          ))}
      </div>
    </div>
  );
};

export default CollectionPreview;
