import {Route, Switch} from 'react-router';
import './App.scss';
import {Header} from './components';
import HomePage from './pages/home-page/HomePage';
import ShopPage from './pages/shop-page/ShopPage';
import SignPage from './pages/sign-page/SignPage';
import {auth, createUserProfileDocument} from './firebase/firebase.utils';
import {useEffect} from 'react';
import {connect, ConnectedProps} from 'react-redux';
import {AppDispatch} from './redux/store';
import {setCurrentUser} from './redux/user/user.action';

const App = (props?: Props) => {
  // componentDidMount
  // useEffect(() => {
  // }, [currentUser]); // // Only re-run the effect if currentUser changes

  useEffect(() => {
    const unsubscribeFromAuth = handleGetFirebase();
    // Clean up the effect
    return () => unsubscribeFromAuth();
  }, []);

  const handleGetFirebase = () => {
    return auth.onAuthStateChanged(async userAuth => {
      if (userAuth) {
        const userRef = await createUserProfileDocument(userAuth, null).then();
        userRef?.onSnapshot(snapshot => {
          setCurrentUser({id: snapshot.id, ...snapshot.data()});
        });
      }
      setCurrentUser(userAuth);
    });
  };
  return (
    <div>
      <Header />
      <Switch>
        <Route exact path="/" component={HomePage} />
        <Route exact path="/shop" component={ShopPage} />
        <Route exact path="/signin" component={SignPage} />
      </Switch>
    </div>
  );
};

const mapDispatchToProps = (dispatch: AppDispatch) => ({
  setCurrentUser: (user: any) => dispatch(setCurrentUser(user)),
});
const connector = connect(mapDispatchToProps, null);
type PropsFromRedux = ConnectedProps<typeof connector>;

type Props = PropsFromRedux & {
  setCurrentUser?: (user: any) => any;
};

export default connector(App);
