import {useState} from 'react';
import './ShopPage.scss';
import {default as SHOP_DATA} from './shop.data';
import {CollectionPreview} from '../../components/index';
const ShopPage = () => {
  // eslint-disable-next-line
  const [collections, setCollections] = useState(SHOP_DATA);
  return (
    <div className="shop-page">
      {collections.map(({id, ...otherCollectionProps}) => (
        <CollectionPreview key={id} {...otherCollectionProps} />
      ))}
    </div>
  );
};

export default ShopPage;
