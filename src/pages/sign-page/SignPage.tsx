import { SignIn, SignUp } from '../../components';
import './SignPage.scss';
const SignPage = () => {
  return <div className="sign-page">
      <SignIn />
      <SignUp />
  </div>;
};

export default SignPage;
