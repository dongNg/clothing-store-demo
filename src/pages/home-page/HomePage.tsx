import {Directory} from '../../components/index';
import './HomePage.scss';
const HomePage = () => {
  return (
    <div className="homepage">
      <Directory />
    </div>
  );
};

export default HomePage;
