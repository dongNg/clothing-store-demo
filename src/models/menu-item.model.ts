import {IBase} from './base.model';

export interface IMenuItem extends IBase {
  size?: string;
  linkUrl?: string;
}
