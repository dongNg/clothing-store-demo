import { ICollectionItem } from "./collection-item.model";
import { IBase } from "./base.model";

export interface ICollectionPreview extends IBase {
  routeName?: string;
  items?: ICollectionItem[];
}
