import {IBase} from './base.model';

export interface ICollectionItem extends IBase {
  name?: string;
  price?: number;
}
