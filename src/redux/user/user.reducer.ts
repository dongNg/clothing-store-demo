import {AnyAction} from 'redux';

const inititalState = {
  currentUser: null,
};

export const userReducer = (state = inititalState, action: AnyAction) => {
  switch (action.type) {
    case 'SET_CURRENT_USER':
      return {
        ...state,
        currentUser: action.payload,
      };
    default:
      return state;
  }
};
