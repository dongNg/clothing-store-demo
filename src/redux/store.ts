import {
  configureStore,
  getDefaultMiddleware,
  StateFromReducersMapObject,
} from '@reduxjs/toolkit';
import {rootReducer} from './root.reducer';
import {DeepPartial} from 'redux';
import {logger} from 'redux-logger';
// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = StateFromReducersMapObject<typeof rootReducer>;

// configureStore === createStore +
export const configureAppStore = (preloadedState?: DeepPartial<RootState>) => {
    const store = configureStore({
        reducer: rootReducer,
        middleware: [logger, ...getDefaultMiddleware()],
        devTools: true,
        preloadedState: preloadedState,
      });

    if(process.env.NODE_ENV !== 'production') {}

    return store;
}


// Infer the `RootState` and `AppDispatch` types from the store itself
type Store = ReturnType<typeof configureAppStore>;
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = Store['dispatch'];
