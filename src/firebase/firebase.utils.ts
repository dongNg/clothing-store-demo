// This import loads the firebase namespace.
import firebase from 'firebase/app';
// These imports load individual services into the firebase namespace.
import 'firebase/firestore';
import 'firebase/auth';
import 'firebase/database';

const firebaseConfig = {
  apiKey: 'AIzaSyAYIGwZp-Qml5a1blZZZiSC4yU0KZI2-CE',
  authDomain: 'clothing-db-51e64.firebaseapp.com',
  databaseUrl: 'https://clothing-db-51e64.firebaseio.com',
  projectId: 'clothing-db-51e64',
  storageBucket: 'clothing-db-51e64.appspot.com',
  messagingSenderId: '886187269314',
  appId: '1:886187269314:web:5e0210c7668816a7ce6f5b',
};

export const createUserProfileDocument = async (
  userAuth: any,
  additionlData: any,
) => {
  if (!userAuth) return;
  const userRef = firestore.doc(`users/${userAuth.uid}`);
  const snapShot = await userRef.get();

  if (!snapShot.exists) {
    const {displayName, email} = userAuth;
    const createAt = new Date();

    try {
      await userRef.set({displayName, email, createAt, ...additionlData});
    } catch (error) {
      console.log('error creatung user', error.message);
    }
  }

  return userRef;
};
firebase.initializeApp(firebaseConfig);

export const auth = firebase.auth();
export const firestore = firebase.firestore();

const provider = new firebase.auth.GoogleAuthProvider();
provider.setCustomParameters({prompt: 'select_account'});
export const signInWithGoogle = () => auth.signInWithPopup(provider);

export default firebase;
